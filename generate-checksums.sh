#!/bin/bash

EXTENSIONS="jar pom"
ALGORITHMS="sha1 md5"

PROGDIR=`dirname $0`
export PATH="$PATH:$PROGDIR"

for ext in $EXTENSIONS
do
	for alg in $ALGORITHMS
	do
		find -type f -name "*.$ext" -exec checksum.sh $alg {} \;
	done
done
