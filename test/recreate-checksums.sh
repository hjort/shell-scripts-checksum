#!/bin/bash

find -name "*.md5" -exec rm -f {} \;
find -name "*.sha1" -exec rm -f {} \;

../generate-checksums.sh

tree
