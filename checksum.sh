#!/bin/bash

if [ $# -ne 2 ]
then
	echo "Usage : checksum [md5|sha1] <file-name>"
	echo "Sample: checksum sha1 /tmp/dir/myfile.jar"
	exit 1
fi

format=$1
file="$2"

if [ ! -f $file ]
then
	echo "File not found: $file"
	exit 2
fi

if [ "$format" == "md5" -o "$format" == "sha1" ]
then
	${format}sum "$file" | cut -d' ' -f1 | tr -d "\n" > "$file.$format"
else
	echo "Please choose a format: md5 or sha1"
	exit 4
fi

echo "Created checksum file $file.$format"
